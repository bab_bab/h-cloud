package com.hcloud.common.core.exception;

/**
 * 主要拥有查询时没有传递报异常
 * @Auther hepangui
 * @Date 2018/10/31
 */
public class IdMissingException extends RuntimeException {
    public IdMissingException() {
    }

    public IdMissingException(String message) {
        super(message);
    }
}
