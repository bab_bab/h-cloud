package com.hcloud.common.core.base;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户
 *
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@Accessors(chain = true)
public class User extends BaseBean {
    private String account;
    private String name;
    private Integer sex;
    private String password;
    private String wechat;
    private String qq;
    private String mobile;
    private String role;
    private Integer state;

}
