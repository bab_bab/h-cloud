
package com.hcloud.auth.server;

import com.hcloud.auth.api.handler.HcloudAccessDeniedHandler;
import com.hcloud.auth.service.impl.HcloudJdbcClientDetailsService;
import com.hcloud.auth.service.impl.HcloudUserDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;

import javax.sql.DataSource;

/**
 * @author hepangui
 * @Date 2018/11/28
 * 授权服务器
 * https://www.cnblogs.com/xingxueliao/p/5911292.html
 * https://blog.csdn.net/u013815546/article/details/76898524
 */
@Configuration
@AllArgsConstructor
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    private final DataSource dataSource;
    private final HcloudUserDetailsService myUserDetailsService;
    private final AuthenticationManager authenticationManager;
    private final WebResponseExceptionTranslator hcloudOAuthExceptionTranslator;
    private final HcloudAccessDeniedHandler hcloudAccessDeniedHandler;
    private final TokenStore tokenStore;
    private final TokenEnhancer tokenEnhancer;


    /**
     * ClientDetailsServiceConfigurer (AuthorizationServerConfigurer 的一个回调配置项)
     * 能够使用内存或者JDBC来实现客户端详情服务（ClientDetailsService），
     * Spring Security OAuth2的配置方法是编写@Configuration类继承AuthorizationServerConfigurerAdapter，
     * 然后重写void configure(ClientDetailsServiceConfigurer clients)方法，
     *
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(new HcloudJdbcClientDetailsService(dataSource));
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
        //enable client to get the authenticated when using the /oauth/token to get a access token
        //there is a 401 authentication is required if it doesn't allow form authentication for clients when access /oauth/token
        oauthServer
                .allowFormAuthenticationForClients()
                // 开启/oauth/token_key验证端口无权限访问
                .tokenKeyAccess("permitAll()")
                // 开启/oauth/check_token验证端口认证权限访问
                .checkTokenAccess("permitAll()")
                .accessDeniedHandler(hcloudAccessDeniedHandler);
    }

    /**
     * jwtAccessTokenConverter是用来生成token的转换器，而token令牌默认是有签名的，且资源服务器需要验证这个签名
     *
     * @param endpoints
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .tokenEnhancer(tokenEnhancer)//增强器
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
//                .tokenServices(tokenServices)
                .tokenStore(tokenStore)
                .authenticationManager(authenticationManager)
                .reuseRefreshTokens(false).userDetailsService(myUserDetailsService)
                .exceptionTranslator(hcloudOAuthExceptionTranslator);

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
