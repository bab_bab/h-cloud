package com.hcloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author hepangui
 * @date 2018年10月26日
 * 服务中心
 */
@EnableEurekaServer
@SpringBootApplication
public class HCloudEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HCloudEurekaApplication.class, args);
	}
}
