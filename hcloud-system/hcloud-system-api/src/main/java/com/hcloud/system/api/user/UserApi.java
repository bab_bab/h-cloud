package com.hcloud.system.api.user;

import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.base.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
public interface UserApi {
    @GetMapping("/user/getByAccount/{account}")
    HCloudResult<User> getUserByAccount(@PathVariable("account") String account);

    @GetMapping("/user/getByMobile/{para}")
    HCloudResult<User> getUserByMobile(@PathVariable("para") String para);

    @GetMapping("/user/getByQq/{para}")
    HCloudResult<User> getUserByQq(@PathVariable("para") String para);

    @GetMapping("/user/getByWeixin/{para}")
    HCloudResult<User> getUserByWeixin(@PathVariable("para") String para);
}
