package com.hcloud.system.api.authority;

import com.hcloud.common.core.base.HCloudResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
public interface AuthorityApi {
    @PostMapping (value = "/role/findAuthorityByRoleIds")
    HCloudResult<List<String>> findAuthorityByRoles(@RequestParam("roleIds") String roleIds);
}
