package com.hcloud.system.user.controller;

import com.hcloud.auth.api.util.AuthUtil;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.annontion.OperateLog;
import com.hcloud.common.core.annontion.OperatePostfix;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.base.User;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.core.constants.OperateType;
import com.hcloud.common.crud.annon.HCloudPreAuthorize;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.common.social.service.QQInfoService;
import com.hcloud.common.social.service.WeixinInfoService;
import com.hcloud.system.api.user.UserApi;
import com.hcloud.system.user.entity.UserEntity;
import com.hcloud.system.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@AllArgsConstructor
@AuthPrefix(AuthConstants.SYSTEM_USER_PREFIX)
@OperatePostfix("用户")
public class UserController extends BaseDataController<UserEntity> implements UserApi {

    private final UserService userService;

    private WeixinInfoService weixinInfoService;
    private QQInfoService qqInfoService;

    @Override
    public BaseDataService getBaseDataService() {
        return userService;
    }

    @Override
    public HCloudResult getUserByAccount(@PathVariable String account) {
        return new HCloudResult(userService.findByAccount(account));
    }

    @Override
    public HCloudResult getUserByMobile(@PathVariable String para) {
        return new HCloudResult(userService.findByMobile(para));
    }
    @Override
    public HCloudResult getUserByWeixin(@PathVariable String para) {
        return new HCloudResult(userService.findByWeixin(para));
    }
    @Override
    public HCloudResult getUserByQq(@PathVariable String para) {
        return new HCloudResult(userService.findByQq(para));
    }


    @PostMapping("/user/modifyPass")
    @OperateLog(title = "修改密码", type = OperateType.UPDATE)
    public HCloudResult modifyPass(String oldPsw, String newPsw) {
        User user = AuthUtil.getUser();
        this.userService.modifyPass(user, oldPsw, newPsw);
        return new HCloudResult();
    }

    @PostMapping("/user/modifyInfo")
    @OperateLog(title = "修改个人信息", type = OperateType.UPDATE)
    public HCloudResult modifyInfo(UserEntity user) {
        this.userService.update(user);
        return new HCloudResult();
    }

    @PostMapping("/user/bind/weixin")
    @OperateLog(title = "绑定微信", type = OperateType.UPDATE)
    public HCloudResult binWeixin(String code) {
        String openId = null;
        try {
            openId = this.weixinInfoService.getOpenId(code);
        } catch (Exception e) {
            return new HCloudResult("绑定微信失败");
        }
        User user = AuthUtil.getUser();
        UserEntity userEntity = this.userService.get(user.getId());
        if (userEntity != null) {
            userEntity.setWechat(openId);
            this.userService.update(userEntity);
        }
        return new HCloudResult();
    }

    @PostMapping("/user/bind/qq")
    @OperateLog(title = "绑定qq", type = OperateType.UPDATE)
    public HCloudResult bindQq(String code) {
        String openId = null;
        try {
            openId = this.qqInfoService.getOpenId(code);
        } catch (Exception e) {
            return new HCloudResult("绑定qq失败");
        }
        User user = AuthUtil.getUser();
        UserEntity userEntity = this.userService.get(user.getId());
        if (userEntity != null) {
            userEntity.setQq(openId);
            this.userService.update(userEntity);
        }
        return new HCloudResult();
    }


    @PostMapping("/user/resetPass")
    @OperateLog(title = "重置密码", type = OperateType.UPDATE)
    @HCloudPreAuthorize(AuthConstants.EDIT)
    public HCloudResult resetPass(String userId) {
        this.userService.resetPass(userId);
        return new HCloudResult();
    }
}
