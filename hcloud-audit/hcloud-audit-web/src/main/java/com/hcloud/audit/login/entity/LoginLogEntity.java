package com.hcloud.audit.login.entity;

import com.hcloud.common.crud.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "h_audit_login_log",
        indexes = {@Index(name = "idx_name", columnList = "username")}
)
public class LoginLogEntity extends BaseEntity {
    private String account;
    private String loginType;
    private String username;
    private String osName;
    private String device;
    private String browser;
    private String ipAddr;
    private Integer success;
    private String msg;

    @Transient
    private String usernameLike;
    @Transient
    private Date createTimeLt;
    @Transient
    private Date createTimeGt;

}
